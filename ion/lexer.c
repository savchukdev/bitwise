// list of keyword pointers
const char *typedef_keyword;
const char *enum_keyword;
const char *struct_keyword;
const char *union_keyword;
const char *var_keyword;
const char *const_keyword;
const char *func_keyword;
const char *sizeof_keyword;
const char *break_keyword;
const char *continue_keyword;
const char *return_keyword;
const char *if_keyword;
const char *else_keyword;
const char *while_keyword;
const char *do_keyword;
const char *for_keyword;
const char *switch_keyword;
const char *case_keyword;
const char *default_keyword;

const char *first_keyword;
const char *last_keyword;
// dynamic array of keywords. gets filled with each KEYWORD macro call
const char **keywords = NULL;

#define KEYWORD(name) name##_keyword = str_intern(#name); array_push(keywords, name##_keyword)

void init_keywords() {
  static bool initialized = false;
  if (initialized) {
    return;
  }

  char *arena_end = string_arena.end;
  KEYWORD(typedef);
  KEYWORD(enum);
  KEYWORD(struct);
  KEYWORD(union);
  KEYWORD(var);
  KEYWORD(const);
  KEYWORD(func);
  KEYWORD(sizeof);
  KEYWORD(break);
  KEYWORD(continue);
  KEYWORD(return);
  KEYWORD(if);
  KEYWORD(else);
  KEYWORD(while);
  KEYWORD(do);
  KEYWORD(for);
  KEYWORD(switch);
  KEYWORD(case);
  KEYWORD(default);

  assert(string_arena.end == arena_end);
  first_keyword = typedef_keyword;
  last_keyword = default_keyword;

  initialized = true;
}

#undef KEYWORD

bool is_keyword_string(const char *string) {
  return string >= first_keyword && string <= last_keyword;
}

typedef enum TokenKind {
  TOKEN_EOF,
  TOKEN_COLON,
  TOKEN_LPAREN,
  TOKEN_RPAREN,
  TOKEN_LBRACE,
  TOKEN_RBRACE,
  TOKEN_LBRACKET,
  TOKEN_RBRACKET,
  TOKEN_COMMA,
  TOKEN_DOT,
  TOKEN_QUESTION,
  TOKEN_SEMICOLON,
  TOKEN_BANG,
  TOKEN_LAST_CHAR = 127,

  TOKEN_INTEGER,
  TOKEN_FLOAT,
  TOKEN_IDENTIFIER,
  TOKEN_KEYWORD,
  TOKEN_STRING,
  // Multiplicative precedence
  TOKEN_MUL,
  TOKEN_FIRST_MUL = TOKEN_MUL,
  TOKEN_DIV,
  TOKEN_MOD,
  TOKEN_LSHIFT,
  TOKEN_RSHIFT,
  TOKEN_BINARY_AND,
  TOKEN_LAST_MUL = TOKEN_BINARY_AND,
  // Additive precedence
  TOKEN_ADD,
  TOKEN_FIRST_ADD = TOKEN_ADD,
  TOKEN_SUB,
  TOKEN_XOR,
  TOKEN_BINARY_OR,
  TOKEN_LAST_ADD = TOKEN_BINARY_OR,
  // Comparative precedence
  TOKEN_EQ,
  TOKEN_FIRST_CMP = TOKEN_EQ,
  TOKEN_NOTEQ,
  TOKEN_LT,
  TOKEN_LTEQ,
  TOKEN_GT,
  TOKEN_GTEQ,
  TOKEN_LAST_CMP = TOKEN_GTEQ,

  TOKEN_AND,
  TOKEN_OR,
  TOKEN_INC,
  TOKEN_DEC,

  TOKEN_ASSIGN,
  TOKEN_FIRST_ASSIGN = TOKEN_ASSIGN,
  TOKEN_COLON_ASSIGN,
  TOKEN_ADD_ASSIGN,
  TOKEN_SUB_ASSIGN,
  TOKEN_OR_ASSIGN,
  TOKEN_AND_ASSIGN,
  TOKEN_XOR_ASSIGN,
  TOKEN_LSHIFT_ASSIGN,
  TOKEN_RSHIFT_ASSIGN,
  TOKEN_MUL_ASSIGN,
  TOKEN_DIV_ASSIGN,
  TOKEN_MOD_ASSIGN,
  TOKEN_LAST_ASSIGN = TOKEN_MOD_ASSIGN,
} TokenKind;

typedef enum TokenMod {
  TOKENMOD_NONE,
  TOKENMOD_HEX,
  TOKENMOD_OCT,
  TOKENMOD_BIN,
  TOKENMOD_CHAR,
} TokenMod;

char to_token_kind(char ch) {
  return (TokenKind)ch;
}

typedef struct Token {
  TokenKind kind;
  TokenMod mod;
  char *start;
  char *end;
  union {
    int int_value;
    double float_value;
    const char* identifier;
    const char* string_value;
  };
} Token;

char* stream;
Token token;


const char *token_kind_names[] = {
  [TOKEN_EOF] = "EOF",
  [TOKEN_COLON] = ":",
  [TOKEN_LPAREN] = "(",
  [TOKEN_RPAREN] = ")",
  [TOKEN_LBRACE] = "{",
  [TOKEN_RBRACE] = "}",
  [TOKEN_LBRACKET] = "[",
  [TOKEN_RBRACKET] = "]",
  [TOKEN_COMMA] = ",",
  [TOKEN_DOT] = ".",
  [TOKEN_QUESTION] = "?",
  [TOKEN_SEMICOLON] = ";",
  [TOKEN_BANG] = "!",
  [TOKEN_INTEGER] = "int",
  [TOKEN_IDENTIFIER] = "name",
  [TOKEN_FLOAT] = "float",
  [TOKEN_STRING] = "string",
  [TOKEN_MUL] = "*",
  [TOKEN_DIV] = "/",
  [TOKEN_MOD] = "%",
  [TOKEN_BINARY_AND] = "&",
  [TOKEN_ADD] = "+",
  [TOKEN_SUB] = "-",
  [TOKEN_XOR] = "^",
  [TOKEN_BINARY_OR] = "|",
  [TOKEN_EQ] = "==",
  [TOKEN_NOTEQ] = "!=",
  [TOKEN_LTEQ] = "<=",
  [TOKEN_GTEQ] = ">=",
  [TOKEN_AND] = "&&",
  [TOKEN_OR] = "||",
  [TOKEN_INC] = "++",
  [TOKEN_DEC] = "--",
  [TOKEN_LT] = "<",
  [TOKEN_GT] = ">",
  [TOKEN_LSHIFT] = "<<",
  [TOKEN_RSHIFT] = ">>",
  [TOKEN_COLON_ASSIGN] = ":=",
  [TOKEN_ADD_ASSIGN] = "+=",
  [TOKEN_SUB_ASSIGN] = "-=",
  [TOKEN_OR_ASSIGN] = "|=",
  [TOKEN_AND_ASSIGN] = "&=",
  [TOKEN_XOR_ASSIGN] = "^=",
  [TOKEN_MUL_ASSIGN] = "*=",
  [TOKEN_DIV_ASSIGN] = "/=",
  [TOKEN_MOD_ASSIGN] = "%=",
  [TOKEN_LSHIFT_ASSIGN] = "<<=",
  [TOKEN_RSHIFT_ASSIGN] = ">>=",
};

uint8_t char_to_digit[256] = {
  ['0'] = 0,
  ['1'] = 1,
  ['2'] = 2,
  ['3'] = 3,
  ['4'] = 4,
  ['5'] = 5,
  ['6'] = 6,
  ['7'] = 7,
  ['8'] = 8,
  ['9'] = 9,
  ['a'] = 10,
  ['A'] = 10,
  ['b'] = 11,
  ['B'] = 11,
  ['c'] = 12,
  ['C'] = 12,
  ['d'] = 13,
  ['D'] = 13,
  ['e'] = 14,
  ['E'] = 14,
  ['f'] = 15,
  ['F'] = 15,
};

void scan_int() {
  int base = 10;
  if (*stream == '0') {
    stream++;
    if (tolower(*stream) == 'x') {
      token.mod = TOKENMOD_HEX;
      base = 16;
      stream++;
    } else if (tolower(*stream) == 'o') {
      token.mod = TOKENMOD_OCT;
      base = 8;
      stream++;
    } else if (tolower(*stream) == 'b') {
      token.mod = TOKENMOD_BIN;
      base = 2;
      stream++;
    }
  }

  int val = 0;
  while(true) {
    int digit = char_to_digit[(size_t)*stream];
    if (digit == 0 && *stream != '0') {
      break;
    }
    if (digit >= base) {
      syntax_error("Digit '%c' is out of range for base %d", *stream, base);
    }
    if (val > (INT32_MAX - digit) / base) {
      syntax_error("Integer overflow literal");
      while(isdigit(*stream)) {
        stream++;
      }
      val = 0;
    }
    val = val * base + digit;
    stream++;
  }

  token.kind = TOKEN_INTEGER;
  token.int_value = val;
}

// format: [0-9]*[.][0-9]*([eE][+-]?[0-9]+)?
void scan_float() {
  char* start = stream;
  while(isdigit(*stream)) {
    stream++;
  }

  if (*stream == '.') {
    stream++;
  }

  while(isdigit(*stream)) {
    stream++;
  }

  if (tolower(*stream) == 'e') {
    stream++;
    if (*stream == '+' || *stream == '-') {
      stream++;

      if (!isdigit(*stream)) {
        syntax_error("Expect digit after float literal exponent, found '%c'", *stream);
      }

      while(isdigit(*stream)) {
        stream++;
      }
    }
  }

  double result = strtod(start, &stream);

  if (result == HUGE_VAL) {
    syntax_error("Float literal overflow");
  }

  token.kind = TOKEN_FLOAT;
  token.float_value = result;
}

char escape_to_char[256] = {
  ['n'] = '\n',
  ['t'] = '\t',
  ['v'] = '\v',
  ['r'] = '\r',
  ['b'] = '\b',
  ['a'] = '\a',
  ['0'] = 0,
};

void scan_char() {
  assert(*stream == '\'');
  stream++;

  int value = 0;

  if (*stream == '\'') {
    syntax_error("Character literal can't be empty");
  } else if (*stream == '\n') {
    syntax_error("Character literal can't be a new line");
  } else if (*stream == '\\') {
    stream++;
    value = escape_to_char[(size_t)*stream];
    if (value == 0 && *stream != '0') {
      syntax_error("Invalid escape sequence '\\%c'", *stream);
    }
  } else {
    value = *stream;
  }
  stream++;

  if (*stream != '\'') {
    syntax_error("Expected ' (closing char quote), got '%c'", *stream);
  } else {
    stream++;
  }

  token.kind = TOKEN_INTEGER;
  token.mod = TOKENMOD_CHAR;
  token.int_value = value;
}

void scan_str() {
  assert(*stream == '"');
  stream++;

  char* string = 0;

  while(*stream && *stream != '"') {
    char ch = *stream;
    if (ch == '\n') {
      syntax_error("Character literal can't be a new line");
    } else if (ch == '\\') {
      stream++;
      ch = escape_to_char[(size_t)*stream];
      if (ch == 0 && *stream != '0') {
        syntax_error("Invalid escape sequence '\\%c'", *stream);
      }
    }
    stream++;
    array_push(string, ch);
  }

  if (*stream) {
    assert(*stream == '"');
    stream++;
  } else {
    syntax_error("Unterminated string literal");
  }

  array_push(string, 0);

  token.kind = TOKEN_STRING;
  token.string_value = string;
}

#define MATCH_CASE1(c1, k1) \
  case c1: \
{ \
  token.kind = k1; \
  stream++; \
} break

#define MATCH_CASE2(c1, k1, c2, k2) \
  case c1: \
{ \
  token.kind = k1; \
  stream++; \
  if (*stream == c2) { \
    token.kind = k2; \
    stream++; \
  } \
} break

#define MATCH_CASE3(c1, k1, c2, k2, c3, k3) \
  case c1: \
{ \
  token.kind = k1; \
  stream++; \
  if (*stream == c2) { \
    token.kind = k2; \
    stream++; \
  } else if (*stream == c3) { \
    token.kind = k3; \
    stream++; \
  } \
} break

void next_token() {
repeat:
  init_keywords();
  token.start = stream;
  token.mod = TOKENMOD_NONE;
  switch (*stream) {
    case ' ': case '\n': case '\r': case '\t': case '\v':
      {
        while(isspace(*stream)) {
          stream++;
        }
        goto repeat;
      } break;
    case '\'':
      {
        scan_char();
      } break;
    case '"':
      {
        scan_str();
      } break;
    case '.':
      {
        if (isdigit(stream[1])) {
          scan_float();
        } else {
          token.kind = TOKEN_DOT;
          stream++;
        }
      } break;
    case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
      {
        char* bookmark = stream;
        while(isdigit(*stream)) {
          stream++;
        }

        if (*stream == '.' || tolower(*stream) == 'e') {
          stream = bookmark;
          scan_float();
        } else {
          stream = bookmark;
          scan_int();
        }
      }
      break;
    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j':
    case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't':
    case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J':
    case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T':
    case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
    case '_':
      {
        while(isalnum(*stream) || *stream == '_') {
          stream++;
        }
        token.identifier = str_intern_range(token.start, stream);
        token.kind = is_keyword_string(token.identifier) ? TOKEN_KEYWORD : TOKEN_IDENTIFIER;
      } break;
    case '<':
      {
        token.kind = TOKEN_LT;
        stream++;
        if (*stream == '<') {
          token.kind = TOKEN_LSHIFT;
          stream++;

          if (*stream == '=') {
            token.kind = TOKEN_LSHIFT_ASSIGN;
            stream++;
          }
        } else if (*stream == '=') {
          token.kind = TOKEN_LTEQ;
          stream++;
        }
      } break;
    case '>':
      {
        token.kind = TOKEN_GT;
        stream++;
        if (*stream == '>') {
          token.kind = TOKEN_RSHIFT;
          stream++;

          if (*stream == '=') {
            token.kind = TOKEN_RSHIFT_ASSIGN;
            stream++;
          }
        } else if (*stream == '=') {
          token.kind = TOKEN_GTEQ;
          stream++;
        }
      } break;
      MATCH_CASE3('+', TOKEN_ADD, '=', TOKEN_ADD_ASSIGN, '+', TOKEN_INC);
      MATCH_CASE3('-', TOKEN_SUB, '=', TOKEN_SUB_ASSIGN, '-', TOKEN_DEC);
      MATCH_CASE3('&', TOKEN_BINARY_AND, '=', TOKEN_AND_ASSIGN, '&', TOKEN_AND);
      MATCH_CASE3('|', TOKEN_BINARY_OR, '=', TOKEN_OR_ASSIGN, '|', TOKEN_OR);
      MATCH_CASE2('^', TOKEN_XOR, '=', TOKEN_XOR_ASSIGN);
      MATCH_CASE2('!', TOKEN_BANG, '=', TOKEN_NOTEQ);
      MATCH_CASE2('=', TOKEN_ASSIGN, '=', TOKEN_EQ);
      MATCH_CASE2(':', TOKEN_COLON, '=', TOKEN_COLON_ASSIGN);
      MATCH_CASE2('/', TOKEN_DIV, '=', TOKEN_DIV_ASSIGN);
      MATCH_CASE2('*', TOKEN_MUL, '=', TOKEN_MUL_ASSIGN);
      MATCH_CASE2('%', TOKEN_MOD, '=', TOKEN_MOD_ASSIGN);
      MATCH_CASE1('\0', TOKEN_EOF);
      MATCH_CASE1('(', TOKEN_LPAREN);
      MATCH_CASE1(')', TOKEN_RPAREN);
      MATCH_CASE1('{', TOKEN_LBRACE);
      MATCH_CASE1('}', TOKEN_RBRACE);
      MATCH_CASE1('[', TOKEN_LBRACKET);
      MATCH_CASE1(']', TOKEN_RBRACKET);
      MATCH_CASE1(',', TOKEN_COMMA);
      MATCH_CASE1('?', TOKEN_QUESTION);
      MATCH_CASE1(';', TOKEN_SEMICOLON);
    default:
      syntax_error("Invalid token character '%c', skipping.", *stream);
      stream++;
      goto repeat;
  }
  token.end = stream;
}

#undef MATCH_CASE1
#undef MATCH_CASE2
#undef MATCH_CASE3

bool is_token(TokenKind kind) {
  return token.kind == kind;
}

bool is_token_name(const char* name) {
  return token.kind == TOKEN_IDENTIFIER && token.identifier == name;
}

bool is_keyword(const char *name) {
  return token.kind == TOKEN_KEYWORD && token.identifier == name;
}

bool match_keyword(const char *name) {
  if (is_keyword(name)) {
    next_token();
    return true;
  } else {
    return false;
  }
}

bool match_token(TokenKind kind) {
  if (is_token(kind)) {
    next_token();
    return true;
  } else {
    return false;
  }
}

const char *token_kind_string(TokenKind kind) {
  const char* string = token_kind_names[kind];
  if (string) {
    return string;
  }

  return "unknown";
}

const char *token_info() {
  if (token.kind == TOKEN_KEYWORD || token.kind == TOKEN_IDENTIFIER) {
    return token.identifier;
  } else {
    return token_kind_string(token.kind);
  }
}

static inline bool expect_token(TokenKind kind) {
  if (is_token(kind)) {
    next_token();
    return true;
  } else {
    fatal("expected token %s, got %s", token_info(), token_kind_string(token.kind));
    return false;
  }
}

void init_stream(const char* source) {
  stream = (char*)source;
  next_token();
}

void keywords_test() {
  assert(is_keyword_string(first_keyword));
  assert(is_keyword_string(last_keyword));

  for (const char **it = keywords;
      it != array_end(keywords);
      it++) {
    assert(is_keyword_string(*it));
  }

  assert(!is_keyword_string(str_intern("foo")));
}

#define assert_token(x) (assert(match_token(x)))
#define assert_token_idenfitier(x) (assert(token.identifier == str_intern(x) && match_token(TOKEN_IDENTIFIER)))
#define assert_token_int(i, x) (assert(token.int_value == i && token.mod == x && match_token(TOKEN_INTEGER)))
#define assert_token_float(i) (assert(token.float_value == i && match_token(TOKEN_FLOAT)))
#define assert_token_string(x) (assert(strcmp(token.string_value, x) == 0 && match_token(TOKEN_STRING)))
#define assert_eof() assert_token(TOKEN_EOF)

void lexer_test() {
  keywords_test();

  // ints
  init_stream("0 0b1111 111 453 0o106 0xfF 22 0xCDFB 0b101\0");
  assert_token_int(0, TOKENMOD_NONE);
  assert_token_int(0xF, TOKENMOD_BIN);
  assert_token_int(111, TOKENMOD_NONE);
  assert_token_int(453, TOKENMOD_NONE);
  assert_token_int(70, TOKENMOD_OCT);
  assert_token_int(255, TOKENMOD_HEX);
  assert_token_int(22, TOKENMOD_NONE);
  assert_token_int(0xCDFB, TOKENMOD_HEX);
  assert_token_int(5, TOKENMOD_BIN);
  assert_eof();

  // identifiers
  init_stream("sometin one two\0");
  assert_token_idenfitier("sometin");
  assert_token_idenfitier("one");
  assert_token_idenfitier("two");
  assert_eof();

  // chars
  init_stream("'c' '\t' '\\n'");
  assert_token_int('c', TOKENMOD_CHAR);
  assert_token_int('\t', TOKENMOD_CHAR);
  assert_token_int('\n', TOKENMOD_CHAR);
  assert_eof();

  // strings
  init_stream("\"foo\" \"a\\nb\"");
  assert_token_string("foo");
  assert_token_string("a\nb");
  assert_eof();

  // floats
  init_stream("3.14 5.5 .4e-3 42. 3e10");
  assert_token_float(3.14);
  assert_token_float(5.5);
  assert_token_float(0.0004);
  assert_token_float(42.0);
  assert_token_float(3e10);
  assert_eof();

  // operators
  init_stream("< << <<= <= > >> >>= >=");
  assert_token(TOKEN_LT);
  assert_token(TOKEN_LSHIFT);
  assert_token(TOKEN_LSHIFT_ASSIGN);
  assert_token(TOKEN_LTEQ);

  assert_token(TOKEN_GT);
  assert_token(TOKEN_RSHIFT);
  assert_token(TOKEN_RSHIFT_ASSIGN);
  assert_token(TOKEN_GTEQ);
  assert_eof();

  init_stream("+ += ++ - -= --");
  assert_token(TOKEN_ADD);
  assert_token(TOKEN_ADD_ASSIGN);
  assert_token(TOKEN_INC);

  assert_token(TOKEN_SUB);
  assert_token(TOKEN_SUB_ASSIGN);
  assert_token(TOKEN_DEC);
  assert_eof();

  init_stream("^= != := /= *=");
  assert_token(TOKEN_XOR_ASSIGN);
  assert_token(TOKEN_NOTEQ);
  assert_token(TOKEN_COLON_ASSIGN);
  assert_token(TOKEN_DIV_ASSIGN);
  assert_token(TOKEN_MUL_ASSIGN);
  assert_eof();

  init_stream("&= && |= ||");
  assert_token(TOKEN_AND_ASSIGN);
  assert_token(TOKEN_AND);
  assert_token(TOKEN_OR_ASSIGN);
  assert_token(TOKEN_OR);
  assert_eof();

  // misc
  init_stream("+453-0o106(0xfF)22hey\0");
  assert_token(TOKEN_ADD);
  assert_token_int(453, TOKENMOD_NONE);
  assert_token(TOKEN_SUB);
  assert_token_int(70, TOKENMOD_OCT);
  assert_token(TOKEN_LPAREN);
  assert_token_int(255, TOKENMOD_HEX);
  assert_token(TOKEN_RPAREN);
  assert_token_int(22, TOKENMOD_NONE);
  assert_token_idenfitier("hey");
  assert_eof();
}

#undef assert_token
#undef assert_name
#undef assert_int
#undef assert_eof
