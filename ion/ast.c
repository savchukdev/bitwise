ArenaAllocator ast_arena;

void *ast_alloc(size_t size) {
  assert(size != 0);
  void *ptr = arena_allocator_alloc(&ast_arena, size);
  memset(ptr, 0, size);
  return ptr;
}

void *ast_dup(const void *src, size_t size) {
  if (size == 0) {
    return NULL;
  }

  void *ptr = arena_allocator_alloc(&ast_arena, size);
  memcpy(ptr, src, size);
  return ptr;
}

Typespec *typespec_alloc(TypespecKind kind) {
  Typespec *typespec = ast_alloc(sizeof(*typespec));
  typespec->kind = kind;
  return typespec;
}

Typespec *typespec_identifier_new(const char *name) {
  Typespec* spec = typespec_alloc(TYPESPEC_NAME);
  spec->name = name;

  return spec;
}

Typespec *typespec_pointer_new(Typespec *element) {
  Typespec *spec = typespec_alloc(TYPESPEC_POINTER);
  spec->pointer.element = element;

  return spec;
}

Typespec *typespec_array_new(Typespec *element, Expression *size) {
  Typespec *spec = typespec_alloc(TYPESPEC_ARRAY);
  spec->array.element = element;
  spec->array.size = size;

  return spec;
}

Typespec *typespec_function_new(Typespec **args, size_t args_count, Typespec *ret_type) {
  Typespec *spec = typespec_alloc(TYPESPEC_FUNC);
  spec->func.args = args;
  spec->func.ret_type = ret_type;
  spec->func.args_count = args_count;

  return spec;
}

Expression* expression_alloc(ExpressionKind kind) {
  Expression *expr = xcalloc(1, sizeof(*expr));
  expr->kind = kind;
  return expr;
}

Expression *expression_compound_new(Typespec *type, Expression **args, size_t args_count) {
  Expression *expr = expression_alloc(EXPRESSION_COMPOUND);
  expr->compound.type = type;
  expr->compound.args = args;
  expr->compound.args_count = args_count;

  return expr;
}

Expression *expression_sizeof_expression_new(Expression *expression) {
  Expression *expr = expression_alloc(EXPRESSION_SIZEOF_EXPRESSION);
  expr->sizeof_expression.expression = expression;

  return expr;
}

Expression *expression_sizeof_type_new(Typespec *type) {
  Expression *expr = expression_alloc(EXPRESSION_SIZEOF_TYPE);
  expr->sizeof_type.type = type;

  return expr;
}

Expression* expression_integer_new(uint64_t int_value) {
  Expression *expr = expression_alloc(EXPRESSION_INT);
  expr->integer_value = int_value;
  return expr;
}

Expression* expression_float_new(double float_value) {
  Expression *expr = expression_alloc(EXPRESSION_FLOAT);
  expr->float_value = float_value;
  return expr;
}

Expression* expression_string_new(const char *string) {
  Expression *expr = expression_alloc(EXPRESSION_STRING);
  expr->string_value = string;
  return expr;
}

Expression* expression_identifier_new(const char *name) {
  Expression *expr = expression_alloc(EXPRESSION_IDENTIFIER);
  expr->identifier = name;
  return expr;
}

Expression* expression_cast_new(Typespec *type, Expression *expression) {
  Expression *expr = expression_alloc(EXPRESSION_CAST);
  expr->cast.type = type;
  expr->cast.expression = expression;
  return expr;
}

Expression* expression_call_new(Expression *expression, Expression **args, size_t args_count) {
  Expression *expr = expression_alloc(EXPRESSION_CALL);
  expr->call.expression = expression;
  expr->call.args_count = args_count;
  expr->call.args = args;
  return expr;
}

Expression* expression_index_new(Expression *expression, Expression *index) {
  Expression *expr = expression_alloc(EXPRESSION_INDEX);
  expr->index.expression = expression;
  expr->index.index = index;
  return expr;
}

Expression* expression_field_new(Expression *expression, const char *name) {
  Expression *expr = expression_alloc(EXPRESSION_FIELD);
  expr->field.expression = expression;
  expr->field.name = name;
  return expr;
}

Expression* expression_unary_new(TokenKind operator, Expression *expression) {
  Expression *expr = expression_alloc(EXPRESSION_UNARY);
  expr->unary.operator = operator;
  expr->unary.expression = expression;
  return expr;
}

Expression* expression_binary_new(TokenKind operator, Expression *left, Expression *right) {
  Expression *expr = expression_alloc(EXPRESSION_BINARY);
  expr->binary.operator = operator;
  expr->binary.left = left;
  expr->binary.right = right;
  return expr;
}

Expression* expression_ternary_new(Expression *cond, Expression *then_expr, Expression *else_expr) {
  Expression *expr = expression_alloc(EXPRESSION_TERNARY);
  expr->ternary.condition = cond;
  expr->ternary.then_expression = then_expr;
  expr->ternary.else_expression = else_expr;
  return expr;
}

Declaration *declaration_alloc(DeclarationKind kind, const char *name) {
  Declaration *decl = ast_alloc(sizeof(*decl));
  decl->kind = kind;
  decl->name = name;
  return decl;
}

AggregateItem* aggregate_item_new(const char **names, size_t names_count, Typespec *type) {
  AggregateItem *item = ast_alloc(sizeof(*item));
  item->identifiers = names;
  item->identifiers_count = names_count;
  item->type = type;

  return item;
}

Declaration *declaration_enum_new(EnumItem *items, size_t items_count, const char *name) {
  Declaration *decl = declaration_alloc(DECLARATION_ENUM, name);
  decl->enum_.items = items;
  decl->enum_.items_count = items_count;

  return decl;
}

EnumItem *declaration_enum_item_new(const char *name, Expression *init) {
  EnumItem *item = ast_alloc(sizeof(*item));
  item->name = name;
  item->init = init;

  return item;
}

Declaration *declaration_aggregate_new(DeclarationKind kind, const char *name, AggregateItem *items, size_t items_count) {
  assert(kind == DECLARATION_UNION || kind == DECLARATION_STRUCT);
  Declaration *decl = declaration_alloc(kind, name);
  decl->aggregate.items = items;
  decl->aggregate.items_count = items_count;

  return decl;
}

Declaration *declaration_typedef_new(Typespec *type, const char *name) {
  Declaration *decl = declaration_alloc(DECLARATION_TYPEDEF, name);
  decl->typedef_.type = type;

  return decl;
}

Declaration *declaration_var_new(Typespec *type, Expression *expression, const char *name) {
  Declaration *decl = declaration_alloc(DECLARATION_VAR, name);
  decl->var.type = type;
  decl->var.expression = expression;

  return decl;
}

Declaration *declaration_const_new(Expression *expression, const char *name) {
  Declaration *decl = declaration_alloc(DECLARATION_CONST, name);
  decl->const_.expression = expression;

  return decl;
}

Declaration *declaration_func_new(const char *name, FuncParam *params, size_t params_count, Typespec *ret_type, StatementBlock block) {
  Declaration *decl = declaration_alloc(DECLARATION_FUNC, name);
  decl->function.params = params;
  decl->function.params_count = params_count;
  decl->function.return_type = ret_type;
  decl->function.block = block;

  return decl;
}

Statement *statement_alloc(StatementKind kind) {
  Statement *statement = ast_alloc(sizeof(*statement));
  statement->kind = kind;
  return statement;
}

Statement *statement_return_new(Expression *expr) {
  Statement *statement = statement_alloc(STATEMENT_RETURN);
  statement->return_.expression = expr;

  return statement;
}

Statement *statement_break_new() {
  return statement_alloc(STATEMENT_BREAK);
}

Statement *statement_continue_new() {
  return statement_alloc(STATEMENT_CONTINUE);
}

Statement *statement_if_new(Expression *condition, StatementBlock then_block, ElseIf *elseifs, size_t elseifs_count, StatementBlock else_block) {
  Statement *statement = statement_alloc(STATEMENT_IF);
  statement->if_.condition = condition;
  statement->if_.then_block = then_block;
  statement->if_.elseifs = elseifs;
  statement->if_.elseifs_count = elseifs_count;
  statement->if_.else_block = else_block;

  return statement;
}

Statement *statement_while_new(Expression *condition, StatementBlock block) {
  Statement *statement = statement_alloc(STATEMENT_WHILE);
  statement->while_.condition = condition;
  statement->while_.block = block;

  return statement;
}

Statement *statement_do_while_new(Expression* condition, StatementBlock block) {
  Statement *statement = statement_alloc(STATEMENT_DO_WHILE);
  statement->while_.condition = condition;
  statement->while_.block = block;

  return statement;
}

Statement *statement_for_new(Statement *init, Expression *condition, Statement *next, StatementBlock block) {
  Statement *statement = statement_alloc(STATEMENT_FOR);
  statement->for_.init = init;
  statement->for_.condition = condition;
  statement->for_.next = next;
  statement->for_.block = block;

  return statement;
}

Statement *statement_block_new(StatementBlock block) {
  Statement *statement = statement_alloc(STATEMENT_BLOCK);
  statement->block = block;

  return statement;
}

SwitchCase *switch_case_new(Expression **expressions, size_t expressions_count, StatementBlock block, bool is_default) {
  SwitchCase *swtch = ast_alloc(sizeof(*swtch));
  swtch->expressions = expressions;
  swtch->expressions_count = expressions_count;
  swtch->block = block;
  swtch->is_default = is_default;

  return swtch;
}

Statement *statement_switch_new(Expression *expression, SwitchCase *cases, size_t cases_count) {
  Statement *statement = statement_alloc(STATEMENT_SWITCH);
  statement->switch_.expression = expression;
  statement->switch_.cases = cases;
  statement->switch_.cases_count = cases_count;

  return statement;
}

Statement *statement_assign_new(TokenKind op, Expression *left, Expression *right) {
  Statement *statement = statement_alloc(STATEMENT_ASSIGN);
  statement->assign.operator = op;
  statement->assign.left = left;
  statement->assign.right = right;

  return statement;
}

Statement *statement_init_new(const char *name, Expression *expression) {
  Statement *statement = statement_alloc(STATEMENT_INIT);
  statement->init.name = name;
  statement->init.expression = expression;

  return statement;
}

Statement *statement_expression_new(Expression *expression) {
  Statement *statement = statement_alloc(STATEMENT_EXPRESSION);
  statement->expression = expression;

  return statement;
}

Statement *statement_declaration_new(Declaration *declaration) {
  Statement *statement = statement_alloc(STATEMENT_DECLARATION);
  statement->declaration = declaration;

  return statement;
}
