void print_expression(Expression *expression);
void print_typespec(Typespec *type);
void print_statement(Statement *statement);
void print_declaration(Declaration *declaration);

int print_indent;

void print_newline() {
  printf("\n%.*s", print_indent * 2, "                                                                    ");
}

void print_typespec(Typespec *type) {
  switch(type->kind) {
    case TYPESPEC_NONE:
      {
        assert(0);
        __builtin_unreachable();
      } break;
    case TYPESPEC_NAME:
      {
        printf("%s", type->name);
      } break;
    case TYPESPEC_FUNC:
      {
        FuncTypespec func = type->func;
        printf("(func (");
        for (Typespec **it = func.args; it != func.args + func.args_count; it++) {
          printf(" ");
          print_typespec(*it);
        }
        printf(") ");
        print_typespec(func.ret_type);
        printf(")");
      } break;
    case TYPESPEC_ARRAY:
      {
        printf("(array ");
        print_typespec(type->array.element);
        printf(" ");
        print_expression(type->array.size);
        printf(")");
      } break;
    case TYPESPEC_POINTER:
      {
        printf("(pointer ");
        print_typespec(type->pointer.element);
        printf(")");
      } break;
    default:
      {
        assert(0);
        __builtin_unreachable();
      }
  }
}

void print_expression(Expression *expression) {
  switch(expression->kind) {
    case EXPRESSION_NONE:
      {
        assert(0);
        __builtin_unreachable();
      } break;
    case EXPRESSION_INT:
      {
        printf("%lu", expression->integer_value);
      } break;
    case EXPRESSION_FLOAT:
      {
        printf("%f", expression->float_value);
      } break;
    case EXPRESSION_STRING:
      {
        printf("\"%s\"", expression->string_value);
      } break;
    case EXPRESSION_IDENTIFIER:
      {
        printf("%s", expression->identifier);
      } break;
    case EXPRESSION_CAST:
      {
        printf("(cast ");
        print_typespec(expression->cast.type);
        printf(" ");
        print_expression(expression->cast.expression);
        printf(")");
      } break;
    case EXPRESSION_CALL:
      {
        printf("(");
        print_expression(expression->call.expression);
        for (Expression **it = expression->call.args;
            it != expression->call.args + expression->call.args_count;
            it++) {
          printf(" ");
          print_expression(*it);
        }
        printf(")");
      } break;
    case EXPRESSION_INDEX:
      {
        printf("(index ");
        print_expression(expression->index.expression);
        printf(" ");
        print_expression(expression->index.index);
        printf(")");
      } break;
    case EXPRESSION_FIELD:
      {
        printf("(field ");
        print_expression(expression->field.expression);
        printf(" %s)", expression->field.name);
      } break;
    case EXPRESSION_COMPOUND:
      {
        printf("(compound ");
        if (expression->compound.type) {
          print_typespec(expression->compound.type);
        } else {
          printf("nil");
        }
        for (Expression **it = expression->compound.args;
            it != expression->compound.args + expression->compound.args_count;
            it++) {
          printf(" ");
          print_expression(*it);
        }
        printf(")");
      } break;
    case EXPRESSION_UNARY:
      {
        printf("(%s ", token_kind_string(expression->unary.operator));
        print_expression(expression->unary.expression);
        printf(")");
      } break;
    case EXPRESSION_BINARY:
      {
        printf("(%s ", token_kind_string(expression->binary.operator));
        print_expression(expression->binary.left);
        printf(" ");
        print_expression(expression->binary.right);
        printf(")");
      } break;
    case EXPRESSION_TERNARY:
      {
        printf("(? ");
        print_expression(expression->ternary.condition);
        printf(" ");
        print_expression(expression->ternary.then_expression);
        printf(" ");
        print_expression(expression->ternary.else_expression);
        printf(")");
      } break;
    case EXPRESSION_SIZEOF_TYPE:
      {
        printf("(sizeof ");
        print_typespec(expression->sizeof_type.type);
      } break;
    case EXPRESSION_SIZEOF_EXPRESSION:
      {
        printf("(sizeof ");
        print_expression(expression->sizeof_expression.expression);
      } break;
    default:
      assert(0);
      __builtin_unreachable();
  }
}

void print_statement_block(StatementBlock block) {
  printf("(block ");
  print_indent++;
  for (Statement **it = block.statements;
      it != block.statements + block.statements_count;
      it++) {
    print_newline();
    print_statement(*it);
  }
  print_indent--;
  printf(")");
}

void print_statement(Statement *statement) {
  switch (statement->kind) {
    case STATEMENT_NONE:
      {
        assert(false);
        __builtin_unreachable();
      }
    case STATEMENT_RETURN:
      {
        printf("(return ");
        print_expression(statement->return_.expression);
        printf(")");
      } break;
    case STATEMENT_BREAK:
      {
        printf("(break)");
      } break;
    case STATEMENT_CONTINUE:
      {
        printf("(continue)");
      } break;
    case STATEMENT_BLOCK:
      {
        print_statement_block(statement->block);
      } break;
    case STATEMENT_IF:
      {
        printf("(if ");
        print_expression(statement->if_.condition);
        print_indent++;
        print_newline();
        print_statement_block(statement->if_.then_block);

        for (ElseIf *it = statement->if_.elseifs;
            it != statement->if_.elseifs + statement->if_.elseifs_count;
            it++) {
          print_newline();
          printf("elseif ");
          print_expression(it->condition);
          print_newline();
          print_statement_block(it->block);
        }

        if (statement->if_.else_block.statements_count != 0) {
          print_newline();
          printf("else");
          print_newline();
          print_statement_block(statement->if_.else_block);
        }

        print_indent--;
        printf(")");
      } break;
    case STATEMENT_WHILE:
      {
        printf("(while ");
        print_expression(statement->while_.condition);
        print_indent++;
        print_newline();
        print_statement_block(statement->while_.block);
        print_indent--;
        printf(")");
      } break;
    case STATEMENT_DO_WHILE:
      {
        printf("(do-while ");
        print_expression(statement->while_.condition);
        print_indent++;
        print_newline();
        print_statement_block(statement->while_.block);
        print_indent--;
        printf(")");
      } break;
    case STATEMENT_FOR:
      {
        printf("(for ");
        print_statement(statement->for_.init);
        print_expression(statement->for_.condition);
        print_statement(statement->for_.next);
        print_indent++;
        print_newline();
        print_statement_block(statement->for_.block);
        print_indent--;
        printf(")");
      } break;
    case STATEMENT_SWITCH:
      {
        printf("(switch ");
        print_expression(statement->switch_.expression);
        print_indent++;
        for (SwitchCase *it = statement->switch_.cases;
            it != statement->switch_.cases + statement->switch_.cases_count;
            it++) {
          print_newline();
          printf("(case (%s", it->is_default ? " default " : "");
          for (Expression **expr = it->expressions;
              expr != it->expressions + it->expressions_count;
              expr++) {
            printf(" ");
            print_expression(*expr);
            printf(" ");
          }
          printf(")");
          print_indent++;
          print_newline();
          print_statement_block(it->block);
          print_indent--;
        }
        print_indent--;
        printf(")");
      } break;
    case STATEMENT_ASSIGN:
      {
        printf("(%s ", token_kind_string(statement->assign.operator));
        print_expression(statement->assign.left);
        if (statement->assign.right) {
          printf(" ");
          print_expression(statement->assign.right);
        }
        printf(")");
      } break;
    case STATEMENT_INIT:
      {
        printf("(:= %s ", statement->init.name);
        print_expression(statement->init.expression);
        printf(")");
      } break;
    case STATEMENT_EXPRESSION:
      {
        print_expression(statement->expression);
      } break;
    case STATEMENT_DECLARATION:
      {
        print_declaration(statement->declaration);
      } break;
    default:
      {
        assert(false);
        __builtin_unreachable();
      }
  }
}

void print_aggregate_declaration(Declaration *declaration) {
  for (AggregateItem *it = declaration->aggregate.items;
    it != declaration->aggregate.items + declaration->aggregate.items_count;
    it++) {
    print_newline();
    printf("(");
    print_typespec(it->type);
    for (const char **name = it->identifiers;
        name != it->identifiers + it->identifiers_count;
        name++) {
      printf(" %s", *name);
    }
    printf(")");
  }
}

void print_declaration(Declaration *declaration) {
  switch(declaration->kind) {
    case DECLARATION_NONE:
      {
        assert(false);
        __builtin_unreachable();
      } break;
    case DECLARATION_ENUM:
      {
        printf("(enum %s", declaration->name);
        print_indent++;
        for (EnumItem *it = declaration->enum_.items;
            it != declaration->enum_.items + declaration->enum_.items_count;
            it++) {
          print_newline();
          printf("(%s ", it->name);
          if (it->init) {
            print_expression(it->init);
          } else {
            printf("nil");
          }
          printf(")");
        }
        print_indent--;
        printf(")");
      } break;
    case DECLARATION_STRUCT:
      {
        printf("(struct %s", declaration->name);
        print_indent++;
        print_aggregate_declaration(declaration);
        print_indent--;
        printf(")");
      } break;
    case DECLARATION_UNION:
      {
        printf("(union %s", declaration->name);
        print_indent++;
        print_aggregate_declaration(declaration);
        print_indent--;
        printf(")");
      } break;
    case DECLARATION_VAR:
      {
        printf("(var %s ", declaration->name);
        if (declaration->var.type) {
          print_typespec(declaration->var.type);
        } else {
          printf("nil");
        }
        printf(" ");
        print_expression(declaration->var.expression);
        printf(")");
      } break;
    case DECLARATION_CONST:
      {
        printf("(const %s", declaration->name);
        print_expression(declaration->const_.expression);
        printf(")");
      } break;
    case DECLARATION_TYPEDEF:
      {
        printf("(typedef %s", declaration->name);
        print_typespec(declaration->typedef_.type);
        printf(")");
      } break;
    case DECLARATION_FUNC:
      {
        printf("(func %s", declaration->name);
        printf("(");
        for (FuncParam *it = declaration->function.params;
            it != declaration->function.params + declaration->function.params_count;
            it++) {
          printf(" %s ", it->name);
          print_typespec(it->type);
        }
        printf(")");

        if (declaration->function.return_type) {
          print_typespec(declaration->function.return_type);
        } else {
          printf("nil");
        }

        print_indent++;
        print_newline();
        print_statement_block(declaration->function.block);
        print_indent--;

        printf(")");
      } break;
    default:
      {
        assert(false);
        __builtin_unreachable();
      }
  }
}

void print_expression_line(Expression* expression) {
  print_expression(expression);
  printf("\n");
}

void print_test() {
  Expression *expressions[] = {
    expression_binary_new('+', expression_integer_new(1), expression_integer_new(2)),
    expression_unary_new('-', expression_float_new(3.14)),
    expression_ternary_new(
      expression_identifier_new("flag"),
      expression_string_new("true"),
      expression_string_new("false")
    ),
    expression_field_new(expression_identifier_new("person"), "name"),
    expression_call_new(
      expression_identifier_new("fact"),
      (Expression*[]){expression_integer_new(42)},
      1
    ),
    expression_index_new(
      expression_field_new(expression_identifier_new("person"), "siblings"),
      expression_integer_new(0)
    ),
    expression_cast_new(
      typespec_pointer_new(typespec_identifier_new("int")),
      expression_identifier_new("void_ptr")
    ),
  };

  for (Expression **it = expressions;
    it != expressions + sizeof(expressions) / sizeof(*expressions);
    it++) {
    print_expression_line(*it);
  }
  printf("\n");

  Statement *statements[] = {
    statement_return_new(expression_integer_new(3)),
    statement_break_new(),
    statement_continue_new(),
    statement_if_new(
      expression_binary_new('>', expression_integer_new(4), expression_integer_new(2)),
      (StatementBlock){
        (Statement*[]){
          statement_return_new(expression_integer_new(0)),
        },
        1
      },
      (ElseIf[]){
        {
          expression_identifier_new("flag2"),
          (StatementBlock){
            (Statement*[]){
              statement_return_new(expression_integer_new(1)),
            },
            1
          },
        }
      },
      1,
      (StatementBlock){
        (Statement*[]){
          statement_return_new(expression_integer_new(2)),
        },
        1
      }
    ),
    statement_expression_new(
      expression_call_new(
        expression_identifier_new("print"),
        (Expression*[]){expression_integer_new(2), expression_integer_new(0)},
        2
      )
    ),
    statement_init_new("x", expression_integer_new(2)),
    statement_while_new(
      expression_identifier_new("name"),
      (StatementBlock){
        (Statement*[]){
          statement_assign_new(TOKEN_ADD_ASSIGN, expression_identifier_new("x"), expression_integer_new(2)),
        },
        1
      }
    ),
    statement_switch_new(
      expression_identifier_new("val"),
      (SwitchCase[]){
      {
        (Expression*[]){
          expression_integer_new(2),
          expression_integer_new(3),
        },
        2,
        (StatementBlock){
          (Statement*[]){
            statement_return_new(expression_integer_new(54))
          },
          1
        },
        false
      },
      {
        (Expression*[]){
          expression_integer_new(10),
        },
        1,
        (StatementBlock){
          (Statement*[]){
            statement_return_new(expression_integer_new(0))
          },
          1
        },
        true
      }
      },
      2
    )
  };

  for (Statement **it = statements;
    it != statements + sizeof(statements) / sizeof(*statements);
    it++) {
    print_statement(*it);
    printf("\n");
  }
}
