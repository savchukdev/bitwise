typedef struct Expression Expression;
typedef struct Statement Statement;
typedef struct Declaration Declaration;
typedef struct Typespec Typespec;

typedef enum TypespecKind {
  TYPESPEC_NONE,
  TYPESPEC_NAME,
  TYPESPEC_FUNC,
  TYPESPEC_ARRAY,
  TYPESPEC_POINTER,
} TypespecKind;

typedef struct FuncTypespec {
  size_t args_count;
  Typespec **args;
  Typespec *ret_type;
} FuncTypespec;

typedef struct PtrTypespec {
  Typespec *element;
} PtrTypespec;

typedef struct ArrayTypespec {
  Typespec *element;
  Expression *size;
} ArrayTypespec;

struct Typespec {
  TypespecKind kind;
  union {
    const char *name;
    FuncTypespec func;
    PtrTypespec pointer;
    ArrayTypespec array;
  };
};
typedef struct AggregateItem {
  const char **identifiers;
  size_t identifiers_count;
  Typespec *type;
} AggregateItem;

typedef struct AggregateDeclaration {
  AggregateItem *items;
  size_t items_count;
} AggregateDeclaration;

typedef struct TypedefDeclaration {
  Typespec *type;
} TypedefDeclaration;

typedef struct VarDeclaration {
  Typespec *type;
  Expression *expression;
} VarDeclaration;

typedef struct ConstDeclaration {
  Expression *expression;
} ConstDeclaration;

typedef struct StatementBlock {
  Statement **statements;
  size_t statements_count;
} StatementBlock;

typedef struct FuncParam {
  const char *name;
  Typespec *type;
} FuncParam;

typedef struct FunctionDeclaration {
  FuncParam *params;
  size_t params_count;
  Typespec *return_type;
  StatementBlock block;
} FunctionDeclaration;

typedef struct EnumItem {
  const char *name;
  Expression *init;
} EnumItem;

typedef struct EnumDeclaration {
  EnumItem *items;
  size_t items_count;
} EnumDeclaration;


typedef enum DeclarationKind {
  DECLARATION_NONE,
  DECLARATION_ENUM,
  DECLARATION_STRUCT,
  DECLARATION_UNION,
  DECLARATION_VAR,
  DECLARATION_CONST,
  DECLARATION_TYPEDEF,
  DECLARATION_FUNC,
} DeclarationKind;

struct Declaration {
  DeclarationKind kind;
  const char *name;
  union {
    EnumDeclaration enum_;
    AggregateDeclaration aggregate;
    FunctionDeclaration function;
    TypedefDeclaration typedef_;
    VarDeclaration var;
    ConstDeclaration const_;
  };
};


typedef struct SizeofExpression {
  Expression *expression;
} SizeofExpression;

typedef struct SizeofExpressionType {
  Typespec *type;
} SizeofExpressionType;

typedef struct CompoundExpression {
  Typespec *type;
  Expression **args;
  size_t args_count;
} CompoundExpression;

typedef struct CastExpression {
  Typespec *type;
  Expression *expression;
} CastExpression;

typedef struct UnaryExpression {
  TokenKind operator;
  Expression *expression;
} UnaryExpression;

typedef struct BinaryExpression {
  TokenKind operator;
  Expression *left;
  Expression *right;
} BinaryExpression;

typedef struct TernaryExpression {
  Expression *condition;
  Expression *then_expression;
  Expression *else_expression;
} TernaryExpression;

typedef struct CallExpression {
  Expression *expression;
  Expression **args;
  size_t args_count;
} CallExpression;

typedef struct IndexExpression {
  Expression *expression;
  Expression *index;
} IndexExpression;

typedef struct FieldExpression {
  Expression *expression;
  const char *name;
} FieldExpression;

typedef enum ExpressionKind {
  EXPRESSION_NONE,
  EXPRESSION_INT,
  EXPRESSION_FLOAT,
  EXPRESSION_STRING,
  EXPRESSION_IDENTIFIER,
  EXPRESSION_CAST,
  EXPRESSION_CALL,
  EXPRESSION_INDEX,
  EXPRESSION_FIELD,
  EXPRESSION_COMPOUND,
  EXPRESSION_UNARY,
  EXPRESSION_BINARY,
  EXPRESSION_TERNARY,
  EXPRESSION_SIZEOF_EXPRESSION,
  EXPRESSION_SIZEOF_TYPE,
} ExpressionKind;

struct Expression {
  ExpressionKind kind;
  union {
    uint64_t integer_value;
    double float_value;
    const char *string_value;
    const char *identifier;
    CompoundExpression compound;
    CastExpression cast;
    UnaryExpression unary;
    BinaryExpression binary;
    TernaryExpression ternary;
    CallExpression call;
    IndexExpression index;
    FieldExpression field;
    SizeofExpression sizeof_expression;
    SizeofExpressionType sizeof_type;
  };
};

typedef struct ElseIf {
  Expression *condition;
  StatementBlock block;
} ElseIf;

typedef struct IfStatement {
  Expression *condition;
  StatementBlock then_block;
  ElseIf *elseifs;
  size_t elseifs_count;
  StatementBlock else_block;
} IfStatement;

typedef struct WhileStatement {
  Expression *condition;
  StatementBlock block;
} WhileStatement;

typedef struct ForStatement {
  Statement *init;
  Expression *condition;
  Statement *next;
  StatementBlock block;
} ForStatement;

typedef struct SwitchCase {
  Expression **expressions;
  size_t expressions_count;
  StatementBlock block;
  bool is_default;
} SwitchCase;

typedef struct SwitchStatement {
  Expression *expression;
  SwitchCase *cases;
  size_t cases_count;
} SwitchStatement;

typedef struct AssignStatement {
  TokenKind operator;
  Expression *left;
  Expression *right;
} AssignStatement;

typedef struct InitStatement {
  const char *name;
  Expression *expression;
} InitStatement;

typedef struct ReturnStatement {
  Expression *expression;
} ReturnStatement;

typedef enum StatementKind {
  STATEMENT_NONE,
  STATEMENT_RETURN,
  STATEMENT_BREAK,
  STATEMENT_CONTINUE,
  STATEMENT_BLOCK,
  STATEMENT_IF,
  STATEMENT_WHILE,
  STATEMENT_DO_WHILE,
  STATEMENT_FOR,
  STATEMENT_SWITCH,
  STATEMENT_ASSIGN,
  STATEMENT_INIT,
  STATEMENT_EXPRESSION,
  STATEMENT_DECLARATION,
} StatementKind;

struct Statement {
  StatementKind kind;
  union {
    ReturnStatement return_;
    IfStatement if_;
    WhileStatement while_;
    ForStatement for_;
    SwitchStatement switch_;
    StatementBlock block;
    AssignStatement assign;
    InitStatement init;
    Expression *expression;
    Declaration *declaration;
  };
};
