#define MAX(x, y) ((x) >= (y) ? (x) : (y))
#define ALIGN_DOWN(n, a) ((n) & ~((a) - 1))
#define ALIGN_UP(n, a) (ALIGN_DOWN((n) + (a) - 1, (a)))
#define ALIGN_DOWN_PTR(p, a) (void*)ALIGN_DOWN((uintptr_t)(p), a)
#define ALIGN_UP_PTR(p, a) (void*)ALIGN_UP((uintptr_t)(p), a)

void* xcalloc(size_t num_elements, size_t item_size) {
  void* ptr = calloc(num_elements, item_size);
  if (!ptr) {
    perror("calloc failed");
    exit(1);
  }

  return ptr;
}

void* xmalloc(size_t size) {
  void* ptr = malloc(size);
  if (!ptr) {
    perror("xmalloc failed");
    exit(1);
  }

  return ptr;
}

void* xrealloc(void* ptr, size_t size) {
  void* new_ptr = realloc(ptr, size);
  if (!new_ptr) {
    perror("xrealloc failed");
    exit(1);
  }

  return new_ptr;
}

void fatal(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);

  fprintf(stderr, "FATAL: ");
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");

  va_end(args);

  exit(1);
}

void syntax_error(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);

  fprintf(stderr, "syntax: ");
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");

  va_end(args);
}

void fatal_syntax_error(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  fprintf(stderr, "syntax: ");
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");

  va_end(args);
  exit(1);
}

typedef struct  {
  size_t length;
  size_t capacity;
  char data[];
} ArrayHeader;

#define array_header(a) \
  ((ArrayHeader*)((char*)a - offsetof(ArrayHeader, data)))

#define __array_fit(a, n) \
  ((n) <= array_capacity(a) ? 0 : ((a) = array_grow((char*)(a), (n), sizeof(*(a)))))

#define array_free(a) \
  (a) ? (free(array_header(a)), (a) = NULL) : 0
#define array_length(a) \
  ((a) ? array_header(a)->length : 0)
#define array_capacity(a) \
  ((a) ? array_header(a)->capacity : 0)
#define array_end(a) ((a) + array_length(a))
#define array_sizeof(a) ((a) ? array_length(a) * sizeof(*(a)) : 0)
#define array_printf(a, ...) ((a) = __array_printf((a), __VA_ARGS__))

#define array_push(a, ...) \
  __array_fit((a), 1 + array_length(a)); (a)[array_header(a)->length++] = (__VA_ARGS__)

#define array_reset_length(a) ((a) ? array_header(a)->length = 0 : 0)

void* array_grow(const char* array, size_t requested_length, size_t element_size) {
  size_t new_capacity = MAX(2 * array_capacity(array), requested_length);
  assert(requested_length <= new_capacity);

  size_t header_offset = offsetof(ArrayHeader, data);
  size_t new_size = new_capacity * element_size + header_offset;

  ArrayHeader* new_array = 0;
  if (array) {
    new_array = xrealloc(array_header(array), new_size);
  } else {
    new_array = xmalloc(new_size);
    new_array->length = 0;
  }

  new_array->capacity = new_capacity;

  return (((char*)new_array) + header_offset);
}

void *__array_printf(char *array, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  int n = vsnprintf(NULL, 0, fmt, args);
  if (array_length(array) == 0) {
    n++; // for zero terminator
  }
  va_end(args);

  __array_fit(array, array_length(array) + n);
  char *destination = array_length(array) == 0 ? array : array_end(array) - 1;
  size_t bytes_to_write = (size_t)array + array_capacity(array) - (size_t)destination;

  va_start(args, fmt);
  vsnprintf(destination, bytes_to_write, fmt, args);
  va_end(args);

  array_header(array)->length += n;

  va_end(args);

  return array;
}

void array_test() {
  int* buf = 0;
  enum { N = 1024 };
  assert(array_length(buf) == 0);
  for (int i = 0; i < N; i++) {
    array_push(buf, i);
  }
  assert(array_length(buf) == N);

  for (size_t i = 0; i < array_length(buf); i++) {
    assert((buf[i]) == (int)i);
  }

  array_free(buf);
  assert(buf == NULL);
  assert(array_length(buf) == 0);

  char *arr = 0;
  array_printf(arr, "something");
  array_printf(arr, " %s %d", "another", 5);
  array_printf(arr, " %d", 10);
  assert(strcmp(arr, "something another 5 10") == 0);

  size_t capacity = array_capacity(arr);

  array_reset_length(arr);
  array_printf(arr, "what");
  assert(strcmp(arr, "what") == 0);
  assert(array_capacity(arr) == capacity);
}


// ===============================
// Arena allocator

typedef struct ArenaAllocator {
  char *ptr;
  char *end;
  char **blocks;
} ArenaAllocator;

#define ARENA_ALLOCATOR_ALIGNMENT 8
#define ARENA_ALLOCATOR_BLOCK_SIZE 1024 * 1024

void arena_allocator_grow(ArenaAllocator *arena, size_t min_size) {
  size_t size = ALIGN_UP(MAX(ARENA_ALLOCATOR_BLOCK_SIZE, min_size), ARENA_ALLOCATOR_ALIGNMENT);
  arena->ptr = xmalloc(size + ARENA_ALLOCATOR_ALIGNMENT);
  arena->end = arena->ptr + size + ARENA_ALLOCATOR_ALIGNMENT;
  array_push(arena->blocks, arena->ptr);
}

void *arena_allocator_alloc(ArenaAllocator* arena, size_t size) {
  assert(arena != NULL);

  if (size > (size_t)(arena->end - arena->ptr)) {
    arena_allocator_grow(arena, size);
    assert(size <= (size_t)(arena->end - arena->ptr));
  }

  void *ptr = arena->ptr;
  arena->ptr = ALIGN_UP_PTR(arena->ptr + size, ARENA_ALLOCATOR_ALIGNMENT);
  assert(arena->ptr < arena->end);
  assert(ptr == ALIGN_DOWN_PTR(ptr, ARENA_ALLOCATOR_ALIGNMENT));
  return ptr;
}

void arena_allocator_free(ArenaAllocator *arena) {
  for (char **it = arena->blocks; it != array_end(arena->blocks); it++) {
    free(*it);
  }
  array_free(arena->blocks);
}

// ===============================
// String interning

typedef struct InternString {
  size_t length;
  const char* data;
} InternString;

static ArenaAllocator string_arena;
static InternString* interns = 0;

const char* str_intern_range(const char* start, const char* end) {
  size_t length = end - start;
  for (InternString *it = interns;
      it != array_end(interns);
      it++) {
    if (it->length == length && strncmp(start, it->data, length) == 0) {
      return it->data;
    }
  }

  char *data = arena_allocator_alloc(&string_arena, length + 1);
  memcpy(data, start, length);
  data[length] = 0;
  InternString intern = { length, data };

  array_push(interns, intern);

  return intern.data;
}

const char* str_intern(const char* string) {
  return str_intern_range(string, string + strlen(string));
}

void str_intern_test() {
  array_free(interns);
  char v1[] = "hello";
  char v2[] = "hello";
  char v3[] = "hello!";
  assert(v1 != v2);

  assert(array_length(interns) == 0);
  str_intern(v1);
  assert(array_length(interns) == 1);
  assert(interns[0].length == 5);
  assert(strncmp(interns[0].data, v1, strlen(v1)) == 0);

  const char* res = str_intern(v2);

  assert(array_length(interns) == 1);
  assert(interns[0].length == 5);
  assert(strncmp(interns[0].data, v1, strlen(v1)) == 0);

  assert(res == interns[0].data);

  assert(str_intern(v1) == str_intern(v2));
  assert(str_intern(v1) != str_intern(v3));

  assert(array_length(interns) == 2);
  assert(interns[1].length == 6);
  assert(strncmp(interns[1].data, v3, strlen(v3)) == 0);

  array_free(interns);
}

void common_test() {
  array_test();
  str_intern_test();
}
