Declaration *parse_declaration();
Declaration *parse_declaration_optional();
Statement *parse_statement();
Expression *parse_expression();
Typespec *parse_typespec();

Typespec *parse_typespec_function() {
  Typespec **args = NULL;
  expect_token(TOKEN_LPAREN);
  if (!is_token(TOKEN_RPAREN)) {
    array_push(args, parse_typespec());
    while(match_token(TOKEN_COMMA)) {
      array_push(args, parse_typespec());
    }
  }
  expect_token(TOKEN_RPAREN);
  Typespec *return_type = NULL;
  if (match_token(TOKEN_COLON)) {
    return_type = parse_typespec();
  }

  return typespec_function_new(ast_dup(args, array_sizeof(args)), array_length(args), return_type);
}

Typespec *parse_typespec_base() {
  if (is_token(TOKEN_IDENTIFIER)) {
    const char *name = token.identifier;
    next_token();
    return typespec_identifier_new(name);
  } else if (match_keyword(func_keyword)) {
    return parse_typespec_function();
  } else if (match_token(TOKEN_LPAREN)) {
    Typespec *type = parse_typespec();
    expect_token(TOKEN_RPAREN);
    return type;
  } else {
    fatal_syntax_error("Unexpected token %s in type", token_kind_string(token.kind));
    __builtin_unreachable();
    return NULL;
  }
}

Typespec *parse_typespec() {
  Typespec *type = parse_typespec_base();
  while (is_token(TOKEN_LBRACKET) || is_token(TOKEN_MUL)) {
    if (match_token(TOKEN_LBRACKET)) {
      Expression *expression = NULL;
      if (!is_token(TOKEN_RBRACKET)) {
        expression = parse_expression();
      }
      expect_token(TOKEN_RBRACKET);
      type = typespec_array_new(type, expression);
    } else {
      assert(is_token(TOKEN_MUL));
      next_token();
      type = typespec_pointer_new(type);
    }
  }

  return type;
}

Expression *parse_compound_expression(Typespec *type) {
  expect_token(TOKEN_LBRACE);
  Expression **args = NULL;
  if (!is_token(TOKEN_RBRACE)) {
    array_push(args, parse_expression());
    while(match_token(TOKEN_COMMA)) {
      array_push(args, parse_expression());
    }
  }
  expect_token(TOKEN_RBRACE);
  return expression_compound_new(type, args, array_length(args));
}

Expression *parse_expression_operand() {
  if (is_token(TOKEN_INTEGER)) {
    uint64_t value = token.int_value;
    next_token();
    return expression_integer_new(value);
  } else if (is_token(TOKEN_FLOAT)) {
    double value = token.float_value;
    next_token();
    return expression_float_new(value);
  } else if (is_token(TOKEN_STRING)) {
    const char* string = token.string_value;
    next_token();
    return expression_string_new(string);
  } else if (is_token(TOKEN_IDENTIFIER)) {
    const char* identifier = token.identifier;
    next_token();
    if (is_token(TOKEN_LBRACE)) {
      return parse_compound_expression(typespec_identifier_new(identifier));
    }
    return expression_identifier_new(identifier);
  } else if (match_keyword(sizeof_keyword)) {
    expect_token(TOKEN_LPAREN);
    if (match_token(TOKEN_COLON)) {
      Typespec *type = parse_typespec();
      expect_token(TOKEN_RPAREN);
      return expression_sizeof_type_new(type);
    } else {
      Expression *expr = parse_expression();
      expect_token(TOKEN_RPAREN);
      return expression_sizeof_expression_new(expr);
    }
  } else if (is_token(TOKEN_LBRACE)) {
    return parse_compound_expression(NULL);
  } else if (match_token(TOKEN_LPAREN)) {
    if (match_token(TOKEN_COLON)) {
      Typespec *type = parse_typespec();
      expect_token(TOKEN_RPAREN);
      return parse_compound_expression(type);
    } else {
      Expression *expr = parse_expression();
      expect_token(TOKEN_RPAREN);
      return expr;
    }
  } else {
    fatal_syntax_error("Unexpected token in expression %s", token_kind_string(token.kind));
    __builtin_unreachable();
    return NULL;
  }
}

Expression* parse_expression_base() {
  Expression *expression = parse_expression_operand();
  while (is_token(TOKEN_LPAREN) || is_token(TOKEN_LBRACKET) || is_token(TOKEN_DOT)) {
    if (match_token(TOKEN_LBRACKET)) {
      Expression *index = parse_expression();
      expect_token(TOKEN_RBRACKET);
      return expression_index_new(expression, index);
    } else if (match_token(TOKEN_LPAREN)) {
      Expression **args = NULL;
      if (!is_token(TOKEN_RPAREN)) {
        array_push(args, parse_expression());
        while(match_token(TOKEN_COMMA)) {
          array_push(args, parse_expression());
        }
      }
      expect_token(TOKEN_RPAREN);
      return expression_call_new(expression, args, array_length(args));
    } else {
      assert(is_token(TOKEN_DOT));
      next_token();
      const char *field = token.identifier;
      expect_token(TOKEN_IDENTIFIER);
      return expression_field_new(expression, field);
    }
  }
  return expression;
}

bool is_unary_opeartor() {
  return is_token(TOKEN_ADD) || is_token(TOKEN_SUB) ||
    is_token(TOKEN_MUL) || is_token(TOKEN_BINARY_AND) ||
    is_token(TOKEN_BANG);
}

Expression *parse_expression_unary() {
  if (is_unary_opeartor()) {
    TokenKind operator = token.kind;
    next_token();
    return expression_unary_new(operator, parse_expression_unary());
  } else {
    return parse_expression_base();
  }
}

bool is_mul_operator() {
  return TOKEN_FIRST_MUL <= token.kind && token.kind <= TOKEN_LAST_MUL;
}

Expression *parse_mul_expression() {
  Expression *expression = parse_expression_unary();
  while (is_mul_operator()) {
    TokenKind operator = token.kind;
    next_token();
    expression = expression_binary_new(operator, expression, parse_expression_unary());
  }

  return expression;
}

bool is_add_operator() {
  return TOKEN_FIRST_ADD <= token.kind && token.kind <= TOKEN_LAST_ADD;
}

Expression *parse_add_expression() {
  Expression *expression = parse_mul_expression();
  while (is_add_operator()) {
    TokenKind operator = token.kind;
    next_token();
    expression = expression_binary_new(operator, expression, parse_mul_expression());
  }

  return expression;
}

bool is_cmp_operator() {
  return TOKEN_FIRST_CMP <= token.kind && token.kind <= TOKEN_LAST_CMP;
}

Expression *parse_cmp_expression() {
  Expression *expression = parse_add_expression();
  while (is_cmp_operator()) {
    TokenKind operator = token.kind;
    next_token();
    expression = expression_binary_new(operator, expression, parse_add_expression());
  }

  return expression;
}

Expression *parse_and_expression() {
  Expression *expression = parse_cmp_expression();
  while (match_token(TOKEN_AND)) {
    expression = expression_binary_new(TOKEN_AND, expression, parse_cmp_expression());
  }

  return expression;
}

Expression *parse_or_expression() {
  Expression *expression = parse_and_expression();
  while (match_token(TOKEN_OR)) {
    expression = expression_binary_new(TOKEN_OR, expression, parse_and_expression());
  }

  return expression;
}

Expression *parse_ternary_expression() {
  Expression *expression = parse_or_expression();
  if (match_token(TOKEN_QUESTION)) {
    Expression *then_expr = parse_ternary_expression();
    expect_token(TOKEN_COLON);
    Expression *else_expr = parse_ternary_expression();

    return expression_ternary_new(expression, then_expr, else_expr);
  }

  return expression;
}

Expression *parse_expression() {
  return parse_ternary_expression();
}

Expression *parse_paren_expression() {
  expect_token(TOKEN_LPAREN);
  Expression *expression = parse_expression();
  expect_token(TOKEN_RPAREN);
  return expression;
}

StatementBlock parse_statement_block() {
  expect_token(TOKEN_LBRACE);
  Statement **statements = NULL;
  while(!is_token(TOKEN_EOF) && !is_token(TOKEN_RBRACE)) {
    array_push(statements, parse_statement());
  }
  expect_token(TOKEN_RBRACE);

  return (StatementBlock){ast_dup(statements, array_sizeof(statements)), array_length(statements)};
}

Statement *parse_statement_if() {
  Expression *condition = parse_paren_expression();
  StatementBlock then_block = parse_statement_block();
  StatementBlock else_block = {0};
  ElseIf *elseifs = NULL;

  while(match_keyword(else_keyword)) {
    if (!match_keyword(if_keyword)) {
      else_block = parse_statement_block();
      break;
    }
    Expression *elseif_condition = parse_paren_expression();
    StatementBlock elseif_body = parse_statement_block();

    array_push(elseifs, (ElseIf){elseif_condition, elseif_body});
  }

  return statement_if_new(
      condition,
      then_block,
      ast_dup(elseifs, array_sizeof(elseifs)),
      array_length(elseifs),
      else_block
  );
}

Statement *parse_statement_while() {
  Expression *condition = parse_paren_expression();
  return statement_while_new(condition, parse_statement_block());
}

Statement *parse_statement_do_while() {
  StatementBlock block = parse_statement_block();
  if (!match_keyword(while_keyword)) {
    fatal_syntax_error("Expected 'while' after 'do' block");
    __builtin_unreachable();
    return NULL;
  }

  Expression *condition = parse_expression();
  Statement *while_statement = statement_do_while_new(condition, block);
  expect_token(TOKEN_SEMICOLON);

  return while_statement;
}

bool is_assign_operator() {
  return TOKEN_FIRST_ASSIGN <= token.kind && token.kind <= TOKEN_LAST_ASSIGN;
}

Statement *parse_simple_statement() {
  Expression *expression = parse_expression();

  if (match_token(TOKEN_COLON_ASSIGN)) {
    if (expression->kind != EXPRESSION_IDENTIFIER) {
      fatal_syntax_error(":= must be preceded by a name");
      return NULL;
    }
    return statement_init_new(expression->identifier, parse_expression());
  } else if (is_assign_operator()) {
    TokenKind operator = token.kind;
    next_token();
    return statement_assign_new(operator, expression, parse_expression());
  } else if (is_token(TOKEN_INC) || is_token(TOKEN_DEC)) {
    TokenKind operator = token.kind;
    next_token();
    return statement_assign_new(operator, expression, NULL);
  } else {
    return statement_expression_new(expression);
  }
}

Statement *parse_statement_for() {
  expect_token(TOKEN_LPAREN);
  Statement *init = NULL;
  if (!is_token(TOKEN_SEMICOLON)) {
    init = parse_simple_statement();
  }
  expect_token(TOKEN_SEMICOLON);

  Expression *condition = NULL;
  if (!is_token(TOKEN_SEMICOLON)) {
    condition = parse_expression();
  }
  expect_token(TOKEN_SEMICOLON);

  Statement *next = NULL;
  if (!is_token(TOKEN_RPAREN)) {
    next = parse_simple_statement();
    if (next->kind == STATEMENT_INIT) {
      fatal_syntax_error("Init statements are not allowed in for statement's next clause");
      return NULL;
    }
  }
  expect_token(TOKEN_RPAREN);

  return statement_for_new(init, condition, next, parse_statement_block());
}

SwitchCase parse_statement_switch_case() {
  Expression **expressions = NULL;
  bool is_default = false;
  while(is_keyword(case_keyword) || is_keyword(default_keyword)) {
    if (match_keyword(case_keyword)) {
      array_push(expressions, parse_expression());
    } else {
      assert(is_keyword(default_keyword));
      next_token();
      if (is_default) {
        fatal_syntax_error("Duplicate default labels in the same switch clause");
      }
      is_default = true;
    }
    expect_token(TOKEN_COLON);
  }
  Statement **statements = NULL;
  while(!is_token(TOKEN_EOF) && !is_token(TOKEN_RBRACE) && !is_keyword(case_keyword) && !is_keyword(default_keyword)) {
    array_push(statements, parse_statement());
  }
  StatementBlock block = {ast_dup(statements, array_sizeof(statements)), array_length(statements)};

  return (SwitchCase){
    ast_dup(expressions, array_sizeof(expressions)),
    array_length(expressions),
    block,
    is_default
  };
}

Statement *parse_statement_switch() {
  Expression *expression = parse_paren_expression();
  SwitchCase *cases = NULL;

  expect_token(TOKEN_LBRACE);
  while(!is_token(TOKEN_EOF) && !is_token(TOKEN_RBRACE)) {
    array_push(cases, parse_statement_switch_case());
  }

  expect_token(TOKEN_RBRACE);
  return statement_switch_new(expression, ast_dup(cases, array_sizeof(cases)), array_length(cases));
}

Statement *parse_statement() {
  if (match_keyword(if_keyword)) {
    return parse_statement_if();
  } else if (match_keyword(while_keyword)) {
    return parse_statement_while();
  } else if (match_keyword(do_keyword)) {
    return parse_statement_do_while();
  } else if (match_keyword(for_keyword)) {
    return parse_statement_for();
  } else if (match_keyword(switch_keyword)) {
    return parse_statement_switch();
  } else if (is_token(TOKEN_LBRACE)) {
    return statement_block_new(parse_statement_block());
  } else if (match_keyword(return_keyword)) {
    Statement *statement = statement_return_new(parse_expression());
    expect_token(TOKEN_SEMICOLON);
    return statement;
  } else if (match_keyword(break_keyword)) {
    expect_token(TOKEN_SEMICOLON);
    return statement_break_new();
  } else if (match_keyword(continue_keyword)) {
    expect_token(TOKEN_SEMICOLON);
    return statement_continue_new();
  } else {
    Declaration *declaration = parse_declaration_optional();
    if (declaration) {
      return statement_declaration_new(declaration);
    }
    Statement *statement = parse_simple_statement();
    expect_token(TOKEN_SEMICOLON);
    return statement;
  }
}

const char *parse_identifier() {
  const char *identifier = token.identifier;
  expect_token(TOKEN_IDENTIFIER);
  return identifier;
}

EnumItem parse_declaration_enum_item() {
  const char *identifier = parse_identifier();
  Expression *init = 0;
  if (match_token(TOKEN_ASSIGN)) {
    init = parse_expression();
  }

  return (EnumItem){identifier, init};
}

Declaration *parse_declaration_enum() {
  const char *identifier = parse_identifier();
  expect_token(TOKEN_LBRACE);
  EnumItem *items = 0;
  if (!is_token(TOKEN_RBRACE)) {
    array_push(items, parse_declaration_enum_item());
    while(match_token(TOKEN_COMMA)) {
      array_push(items, parse_declaration_enum_item());
    }
  }
  expect_token(TOKEN_RBRACE);

  return declaration_enum_new(ast_dup(items, array_sizeof(items)), array_length(items), identifier);
}

AggregateItem parse_aggregate_item() {
  const char **identifiers = 0;
  array_push(identifiers, parse_identifier());
  while(match_token(TOKEN_COMMA)) {
    array_push(identifiers, parse_identifier());
  }

  expect_token(TOKEN_COLON);
  Typespec *type = parse_typespec();
  expect_token(TOKEN_SEMICOLON);

  return (AggregateItem){ast_dup(identifiers, array_sizeof(identifiers)), array_length(identifiers), type};
}

Declaration *parse_declaration_aggregate(DeclarationKind kind) {
  assert(kind == DECLARATION_UNION || kind == DECLARATION_STRUCT);
  const char *identifier = parse_identifier();
  expect_token(TOKEN_LBRACE);
  AggregateItem *items = 0;
  while(!is_token(TOKEN_EOF) && !is_token(TOKEN_RBRACE)) {
    array_push(items, parse_aggregate_item());
  }
  expect_token(TOKEN_RBRACE);

  return declaration_aggregate_new(kind, identifier, ast_dup(items, array_sizeof(items)), array_length(items));
}

Declaration *parse_declaration_var() {
  const char *identifier = parse_identifier();
  if (match_token(TOKEN_ASSIGN)) {
    return declaration_var_new(NULL, parse_expression(), identifier);
  } else if (match_token(TOKEN_COLON)) {
    Typespec *type = parse_typespec();
    Expression *expression = NULL;
    if (match_token(TOKEN_ASSIGN)) {
      expression = parse_expression();
    }

    return declaration_var_new(type, expression, identifier);
  } else {
    fatal_syntax_error("Expect ':' or '=' after var, got %s", token_kind_string(token.kind));
    return 0;
  }
}

Declaration *parse_declaration_const() {
  const char *identifier = parse_identifier();
  expect_token(TOKEN_ASSIGN);
  return declaration_const_new(parse_expression(), identifier);
}

Declaration *parse_declaration_typedef() {
  const char *identifier = parse_identifier();
  expect_token(TOKEN_ASSIGN);
  return declaration_typedef_new(parse_typespec(), identifier);
}

FuncParam parse_func_param() {
  const char *identifier = parse_identifier();
  expect_token(TOKEN_COLON);
  Typespec *type = parse_typespec();

  return (FuncParam){identifier, type};
}

Declaration *parse_declaration_function() {
  const char *identifier = parse_identifier();
  expect_token(TOKEN_LPAREN);

  FuncParam *params = 0;
  if (!is_token(TOKEN_RPAREN)) {
    array_push(params, parse_func_param());
    while(match_token(TOKEN_COMMA)) {
      array_push(params, parse_func_param());
    }
  }

  expect_token(TOKEN_RPAREN);
  Typespec *return_type = 0;
  if (match_token(TOKEN_COLON)) {
    return_type = parse_typespec();
  }

  StatementBlock block = parse_statement_block();

  return declaration_func_new(identifier, ast_dup(params, array_sizeof(params)), array_length(params), return_type, block);
}

Declaration *parse_declaration_optional() {
  if (match_keyword(enum_keyword)) {
    return parse_declaration_enum();
  } else if (match_keyword(struct_keyword)) {
    return parse_declaration_aggregate(DECLARATION_STRUCT);
  } else if (match_keyword(union_keyword)) {
    return parse_declaration_aggregate(DECLARATION_UNION);
  } else if (match_keyword(var_keyword)) {
    return parse_declaration_var();
  } else if (match_keyword(const_keyword)) {
    return parse_declaration_const();
  } else if (match_keyword(typedef_keyword)) {
    return parse_declaration_typedef();
  } else if (match_keyword(func_keyword)) {
    return parse_declaration_function();
  } else {
    return 0;
  }
}

Declaration *parse_declaration() {
  Declaration *declaration = parse_declaration_optional();
  if (!declaration) {
    fatal_syntax_error("Expected declaration keyword, found %s", token_info());
  }

  return declaration;
}

void parse_test() {
  const char *tests[] = {
    "struct Vector { x, y: float; }",
    "enum Color { RED = 3, GREEN, BLUE = 0 }",
    "func f(x: int): bool { switch(x) { case 0: case 1: return true; case 2: default: return false; } }",
    "var foo = a ? a&b + c<<d + e*f == +u-v-w + *g/h(x,y) + -i%k[x] && m <= n*(p+q)/r : 0",
    "func fact(n: int): int { trace(\"fact\"); if (n == 0) { return 1; } else { return n * fact(n-1); } }",
    "const n = sizeof(:int*[16])",
    "const n = sizeof(1+2)",
    "var x = b == 1 ? 1+2 : 3-4",
    "func fact(n: int): int { p := 1; for (i := 1; i <= n; i++) { p *= i; } return p; }",
    "const pi = 3.14",
    "var v = Vector{1.0, -1.0}",
    "var v: Vector = {1.0, -1.0}",
    "union IntOrFloat { i: int; f: float; }",
    "typedef Vectors = Vector[1+2]",
    "func f() { do { print(42); } while(1); }",
    "typedef T = (func(int):int)[16]",
    "func f() { enum E { A, B, C } return 42; }",
  };

  for (const char **it = tests;
      it != tests + sizeof(tests)/sizeof(*tests);
      it++) {
    init_stream(*it);
    Declaration *decl = parse_declaration();
    print_declaration(decl);
    printf("\n");
  }
}
