#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <math.h>

#include "common.c"
#include "lexer.c"
#include "ast.h"
#include "ast.c"
#include "print.c"
#include "parse.c"

void main_test() {
  common_test();
  lexer_test();
  print_test();
  parse_test();

  printf("\x1b[32mAll tests have passed!\x1b[39m\n");
}

int main() {
#if 1
  main_test();
#endif

  return 0;
}
